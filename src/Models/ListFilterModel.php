<?php

namespace Gula\ListFilter\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ListFilterModel extends Model
{
    protected $itemsPerPage = 30;

    public function getList(array $params): array
    {
        $tableName = $params['tableName'];
        $page = (isset($params['page']) ? $params['page'] : 1);
        $listOrder = (isset($params['listOrder']) ? $params['listOrder'] : ['column' => 'id', 'order' => 'ASC']);

        if(isset($params['itemsPerPage'])){
            $this->itemsPerPage = $params['itemsPerPage'];
        }

        //@todo meerdere filters
        //@todo joins
//        $filterText = (isset($params['filterText']) ? $params['filterText'] : null );
        $select = $params['select'];

        $sql = DB::table($tableName);
        $sql->select($params['select']);

        $listCount = $sql->count();

        $sql->orderBy($listOrder['column'], $listOrder['order']);
        $sql->skip((($page - 1) * $this->itemsPerPage));
        $sql->take($this->itemsPerPage);


        $result = $sql->get();

        foreach ($params['select'] as $column){
            $columns[] = [
                'name' => $column,
                'filter' => (in_array($column, $params['filteredColumns']) ? true : false),
            ];
        }

        return [
            'list' => $result,
            'listCount' => $listCount,
            'pages' => ceil($listCount / $this->itemsPerPage),
            'icon' => (isset($params['icon']) ? $params['icon'] : null),
            'title' => strtoupper($params['tableName']),
            'model' => $params['tableName'],
            'columns' => $columns,
            'page' => $page
        ];
    }
}
