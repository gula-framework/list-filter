<?php

namespace Gula\ListFilter;

use Illuminate\Support\ServiceProvider;

class ListFilterServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
//        $this->app->make('Gula\ListFilter\ListFilterController');


    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/views', 'list-filter');
    }
}
