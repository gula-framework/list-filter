@extends('website-cms::base')

@section('content')
    <div class="containerList">
        <div class="row">
            <div class="col-md-12">
                <br/>
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-2">
                                <img src="{{ $list['icon'] }}"/>&nbsp;<b>&nbsp;{{ $list['title'] }}</b>
                            </div>
                            <div class="col-md-10">
                                <a href="/cms/{{$list['model']}}/add" class=""
                                   id="linkAdd">
                                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/add.png" class="symbolRow"/> Regel
                                    Toevoegen
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table table-borderless table-sm table-hover" id="listData">
                            <thead>
                            <tr>
                                @foreach($list['columns'] as $column)
                                    <th>{{$column['name']}}</th>
                                @endforeach
                            </tr>
                            <tr>
                                @foreach($list['columns'] as $column)
                                    @if($column['filter'] === true)
                                        <td><input type="text" class=""
                                                   id="filter_{{$column['name']}}"
                                                   name="searchColumn[]"
                                                   value="*"/>
                                        </td>
                                    @else
                                        <td></td>
                                    @endif
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($list['list'] as $entity)
                                <tr onclick="document.location='/cms/{{$list['model']}}/edit/{{$entity->id}}'"
                                    style="cursor: pointer">
                                    @foreach($list['columns'] as $column)
                                        @php
                                            $name = $column['name'];
                                        @endphp
                                        @if($column['name'] === 'id')
                                            <td align="right">{{$entity->$name}}</td>
                                        @else
                                            <td>{{$entity->$name}}</td>
                                        @endif
                                    @endforeach
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
