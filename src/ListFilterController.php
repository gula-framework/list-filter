<?php

namespace Gula\ListFilter;

use Gula\ListFilter\Models\ListFilterModel;

class ListFilterController
{
    /**
     * @param array $params
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(array $params)
    {
        $tblModel = new ListFilterModel();
        $list = $tblModel->getList($params);

        return  view('list-filter::list', compact('list'));
    }
}
